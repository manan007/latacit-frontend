/* **** Navbar **** */
$(".navbar-toggler").on("click", function() {
    $(".navigation-wrapper").toggleClass("open-navigation");
    $(".navbar-toggler-icon").toggleClass("open-navigation");
    $(".add-fix").toggleClass("open-navigation");
});

$(window).scroll(function() {
    if ($(this).scrollTop() > 150) {
        $("header").addClass("nav-new");
    } else {
        $("header").removeClass("nav-new");
    }
});

$(window).scroll(function() {
    if ($(this).scrollTop() > 20) {
        $(".hero-wrp").addClass("new-hero");
    } else {
        $(".hero-wrp").removeClass("new-hero");
    }
});

/* **** recommends **** */
$(".recommends-slider .multiple-items").slick({
    arrows: false,
    dots: false,
    autoplay: false,
    autoplaySpeed: 1000,
    speed: 1000,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
            },
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
            },
        },
        {
            breakpoint: 575,
            settings: {
                slidesToShow: 1,
            },
        },
        {
            breakpoint: 360,
            settings: {
                slidesToShow: 1,
            },
        },
    ],
});

/* **** recommends **** */
$(".beauty-slider .multiple-items").slick({
    arrows: false,
    dots: false,
    autoplay: false,
    autoplaySpeed: 1000,
    speed: 1000,
    infinite: true,
    centerMode: true,
    centerPadding: "18% 0",
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
            },
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
            },
        },
        {
            breakpoint: 575,
            settings: {
                slidesToShow: 1,
            },
        },
        {
            breakpoint: 360,
            settings: {
                slidesToShow: 1,
            },
        },
    ],
});

$(".keyingredients-slider1 .slider-for").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: ".keyingredients-slider1 .slider-nav",
});
$(".keyingredients-slider1 .slider-nav").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: ".keyingredients-slider1 .slider-for",
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
            },
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 575,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 360,
            settings: {
                slidesToShow: 2,
            },
        },
    ],
});

$(".keyingredients-slider2 .slider-for").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: ".keyingredients-slider2 .slider-nav",
});
$(".keyingredients-slider2 .slider-nav").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: ".keyingredients-slider2 .slider-for",
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
            },
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 575,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 360,
            settings: {
                slidesToShow: 2,
            },
        },
    ],
});

$(".keyingredients-slider3 .slider-for").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: ".keyingredients-slider3 .slider-nav",
});
$(".keyingredients-slider3 .slider-nav").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: ".keyingredients-slider3 .slider-for",
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
            },
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 575,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 360,
            settings: {
                slidesToShow: 2,
            },
        },
    ],
});

/* **** recommends **** */
$(".replenish-slider .single-item").slick({
    arrows: false,
    dots: true,
    autoplay: false,
    autoplaySpeed: 1000,
    speed: 1000,
    infinite: true,
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
});



$(".explore-slider .single-item").slick({
    arrows: false,
    dots: true,
    autoplay: false,
    autoplaySpeed: 1000,
    speed: 1000,
    infinite: true,
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
});


$(".transcendance-slider .single-item").slick({
    arrows: true,
    dots: true,
    autoplay: false,
    autoplaySpeed: 1000,
    speed: 1000,
    infinite: true,
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
});

// envoking-slider

$('.envoking-block-slider').slick({
    centerMode: true,
    arrows: false,
    dots: true,
    autoplay: false,
    autoplaySpeed: 1000,
    speed: 1000,
    infinite: true,
    slidesToScroll: 1,
    slidesToShow: 3,
    responsive: [{
        breakpoint: 768,
        settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 3
        }
    }, {
        breakpoint: 480,
        settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
        }
    }]
});


/* ***** Animetion **** */
AOS.init({
    duration: 800,
});
/* ***** Animetion **** */